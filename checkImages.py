#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 18 19:05:09 2018

@author: leandro
"""

#%%

import pandas as pd
from PIL import Image
from glob import glob
import matplotlib.pyplot as plt
import shutil
import numpy as np

#%%

df = []

for f in glob("/home/leandro/Data/wiki-women/pics/*.jpg"):
#for f in glob("/home/leandro/Data/wiki-women/biggerPics/256x256/*.jpg"):
    img = Image.open(f)
    df.append( img.size )
    
df = pd.DataFrame(df)
df.columns = [ "width", "height" ]
df["file"] = glob("/home/leandro/Data/wiki-women/pics/*.jpg")

#df["colors"] = "#000000"

#%%

df.plot( kind="scatter", x = "width", y = "height", c = "#FF000010")
df.plot( kind="hist", bins = 100 )    

df["width"].sort_values().plot( kind="line" )

plt.plot( range(0, df["width"].shape[0] ), df["width"].sort_values(ascending=False) )
plt.plot( range(0, df["height"].shape[0] ), df["height"].sort_values(ascending=False) )
#df["width"].plot( kind="hist" )    
#df["height"].plot( kind="hist" )    

#%%

dfFiltered = df[ (df["width"] > 400) & (df["height"] > 400) & (df["width"] == df["height"])  ]
#dfFiltered.iloc[[1,2],:]

for r in dfFiltered["file"]:
    shutil.copy(r, "/home/leandro/Data/wiki-women/biggerPics")
    
#%%

np.min( dfFiltered["width"] )

df[ (df["width"] != 256) ]
