#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  9 12:04:51 2018

@author: leandro
"""

import pandas as pd
import plotly.graph_objs as go
from plotly.offline import plot

df = pd.read_csv("datasets/wiki-women/train/dcgan_2018-03-13_20-06-23/log.csv")
df


data = [
    go.Scatter(
        x = df.epoch,
        y = pd.rolling_mean(df.d_loss,1000),
        name = "d_loss"
    ),
    go.Scatter(
        x = df.epoch,
        y = pd.rolling_mean(df.d_acc,1000),
        name = "d_acc"
    ),
    go.Scatter(
        x = df.epoch,
        y = pd.rolling_mean(df.g_loss,1000),
        name = "g_loss"
    ),
]

plot(data)



