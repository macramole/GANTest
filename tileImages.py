#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  7 11:41:17 2018

@author: leandro
"""

#%%

import subprocess
from glob import glob

PATH_IN_SUFFIX = "/home/leandro/Data/yo_128x128/generate"
PATH_IN = [ "1", "2", "3", "4", "5", "6", "7", "8", "9" ]
PATH_OUT = "out_3x3"

TILE = "3x3"

IMAGE_TYPE = "png"

#%%

cantFiles = len ( glob( "%s/%s/*" % (PATH_IN_SUFFIX, PATH_IN[0]) ) )

for i in range(0,cantFiles):
    command = ["montage", "-mode", "concatenate", "-tile", TILE]
    for p in PATH_IN:
        fullPath = "%s/%s" % ( PATH_IN_SUFFIX, p )
        command.append( "%s/%05d.%s" % ( fullPath, i, IMAGE_TYPE ) )
    
    fullPath = "%s/%s" % ( PATH_IN_SUFFIX, PATH_OUT )
    command.append("%s/%05d.%s" % ( fullPath, i, IMAGE_TYPE ))
        
#    print("Executing", command)
    output = subprocess.getoutput(" ".join(command))
#    print(output)
    print(".",end="")

print("DONE :)")